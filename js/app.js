var socket = io('http://localhost:3000');

function sendMessage() {
    var msgEl = document.getElementById("message");
    socket.emit("new-message", msgEl.value);
    msgEl.value = "";
}

var ml = document.getElementById("message-list");

socket.on("message-list", function(newMessage) {
    ml.innerHTML += "<li>" +
        "<img src='https://api.adorable.io/avatars/40/" + newMessage.id + "'>" +
        "" + newMessage.msg +
        "<span class='mini'>" + new Date(newMessage.timestamp) + "</span>" +
        "</li>";
});